#include <iostream>

#define N 4
#define M 4
#define MAX_RANDOM 10

using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");
    srand(time(0));

    // Создаем матрицу
    int **matrix = new int *[N];
    for (int r = 0; r < N; r++) {
        matrix[r] = new int[M];
    }

    int *result = new int[N];

    // Заполняем и выводим матрицу
    cout << "Исходная матрица: " << endl;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < M; c++) {
            matrix[r][c] = rand() % MAX_RANDOM;
            cout << matrix[r][c] << " ";
        }
        cout << endl;
    }


    cout << "Полученный масив:" << endl;
    for (int r = 0; r < N; r++) {
        bool flag = false;
        for (int c = 0; c < M; c++) {
            if(matrix[r][c] % 3 == 0) {
                flag = true;
                break;
            }
        }
        result[r] = flag;
        cout << result[r]<< " ";
    }

    // освобождаем память
    for (int r = 0; r < N; r++) {
        delete[] matrix[r];
    }
    delete[] matrix;
    delete[] result;

    return 0;
}
