#include <iostream>

#define N 4
#define MAX_RANDOM 10

using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");
    srand(time(0));


    // Создаем масив так что бы было побольше нулей
    int **matrix = new int *[N];
    for (int r = 0; r < N; r++) {
        matrix[r] = new int[N];
        for (int c = 0; c < N; c++) {
            matrix[r][c] = rand() % MAX_RANDOM;
        }
    }

    cout << "Исходный массив:" << endl;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            cout << matrix[r][c] << " ";
        }
        cout << endl;
    }

    int count = 0;
    for (int r = 0; r < N; r++) {
        for (int c = N - r - 1; c < N; c++) {
            if (matrix[r][c] == 7) count++;
        }
    }

    cout << "Количество семерок под побочной диагональю: " << count;


    for (int r = 0; r < N; r++) {
        delete[] matrix[r];
    }
    delete[] matrix;

    return 0;
}
