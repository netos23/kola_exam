#include <iostream>


using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");

    int n = 0, m = 0;
    cout << "Введите высоту матрицы:" << endl;
    cin >> n;
    cout << "Введите ширину матрицы:" << endl;
    cin >> m;

    // Создаем матрицу
    int **matrix = new int *[n];
    for (int r = 0; r < n; r++) {
        matrix[r] = new int[m];
    }

    cout << "Полученная матрица:" << endl;
    for(int r = 0; r < n; r++){
        for(int c = 0; c < m; c++){
            matrix[r][c] = r * c;
            cout << matrix[r][c] << " ";
        }
        cout << endl;
    }

    // освобождаем память
    for (int r = 0; r < n; r++) {
        delete[] matrix[r];
    }
    delete[] matrix;

    return 0;
}
