#include <iostream>

#define N 4
#define MAX_RANDOM 10

using namespace std;

void printArr(int *arr, int limit) {
    for (int i = 0; i < limit; i++) {
        cout << arr[i] << " ";
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    srand(time(0));

    // Создаем масив
    int *arr = new int[N];
    // Заполняем масив
    for (int i = 0; i < N; i++) {
        arr[i] = rand() % MAX_RANDOM;
    }
    cout << "Исходный массив:" << endl;
    printArr(arr, N);

    cout << endl << "Введите k: ";
    int k;
    cin >> k;

    printArr(arr, k);

    delete[] arr;
    return 0;
}


