#include <iostream>

#define N 4
#define M 4
#define MAX_RANDOM 1000
#define OFFSET 500

using namespace std;

void deleteMatrix(int **matrix, int h) {
    for (int r = 0; r < h; r++) {
        delete[] matrix[r];
    }
    delete[] matrix;
}

int **createMatrix(int w, int h) {
    int **matrix = new int *[h];
    for (int r = 0; r < h; r++) {
        matrix[r] = new int[w];
        for (int c = 0; c < w; c++) {
            matrix[r][c] = rand() % MAX_RANDOM - OFFSET;
        }
    }
    return matrix;
}

int getSumOfPositiveElements(int **matrix, int w, int h) {
    int sum = 0;
    for (int r = 0; r < h; r++) {
        for (int c = 0; c < w; c++) {
            if (matrix[r][c] > 0) {
                sum += matrix[r][c];
            }
        }
    }
    return sum;
}


void printMatrix(int **matrix, int w, int h) {
    for (int r = 0; r < h; r++) {
        for (int c = 0; c < w; c++) {
            cout << matrix[r][c] << " ";
        }
        cout << endl;
    }
}


int main() {
    setlocale(LC_ALL, "Russian");
    srand(time(0));

    // Создаем матрицу
    int **mat1 = createMatrix(M, N);
    int **mat2 = createMatrix(M, N);

    cout << "Первая матрица:" << endl;
    printMatrix(mat1, M, N);

    cout << "Вторая матрица:" << endl;
    printMatrix(mat2, M, N);

    int sum1 = getSumOfPositiveElements(mat1, M, N);
    int sum2 = getSumOfPositiveElements(mat2, M, N);

    if (sum1 > sum2) {
        cout << "Сумма элементов первой матрицы больше " << sum1 << ">" << sum2;
    } else if (sum1 < sum2) {
        cout << "Сумма элементов второй матрицы больше " << sum1 << "<" << sum2;
    } else {
        cout << "Суммы элементов равны " << sum1 << "=" << sum2;
    }

    // освобождаем память
    deleteMatrix(mat1, N);
    deleteMatrix(mat2, N);

    return 0;
}




