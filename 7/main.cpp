#include <iostream>

#define N 15
#define MAX_RANDOM 40

using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");
    srand(time(0));

    cout << "Исходный массив:" << endl;
    // Создаем масив так что бы было побольше нулей
    int *arr = new int[N];
    for (int i = 0; i < N; i++) {
        arr[i] = rand() % MAX_RANDOM > MAX_RANDOM / 2 ? 0 : rand() % MAX_RANDOM;
        cout << arr[i] << " ";
    }
    cout << endl;


    for (int i = 0; i < N; i++) {
        int offset = 0;
        while (i + offset < N && arr[i + offset] == 0) {
            offset++;
        }

        for (int j = i; j < N - offset; j++) {
            if (j + offset >= N) {
                break;
            }
            arr[j] = arr[j + offset];
        }

        for (int j = N - offset; j < N; j++) {
            arr[j] = 0;
        }
    }


    for (int i = 0; i < N; i++) {
        cout << arr[i] << " ";
    }

    delete[] arr;
    return 0;
}
