#include <iostream>
#include <fstream>
#include <cstring>

#define N 500

using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");

    ifstream fin("../input.txt");

    if (!fin.is_open()) {
        cout << "Файл не может быть открыт!\n";
    } else {
        char str[N];
        fin.getline(str, N);
        fin.close();

        int count = 0;
        const char *target = "нн";
        for (int i = 0; i < strlen(str) - 1; i++) {
            char buf[3];
            buf[0] = str[i];
            buf[1] = str[i + 1];
            buf[2] = '\0';

            if (strcmp(target, buf) == 0) {
                count++;
            }
        }
        cout << "Количество сочетаний нн: " << count;
    }
    return 0;
}
